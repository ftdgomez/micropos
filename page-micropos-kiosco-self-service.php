<?php 

get_header();
$linkheader = "https://danielgomez.website/micropos/wp-content/uploads/2019/05/Banner_Retail.jpg";

?>


<div class="cont fixy">
    <img style="width: 100%;" src="<?php echo get_template_directory_uri(); ?>/img/pics/eee.jpg" alt="banner">
    <h2 class="title">Kiosco Self Services</h2>
    <p class="" style="">
        Nuestro kiosco es adaptable para cualquier tipo de  negocio que realice ventas de productos consumible o no consumible.
    <br><br>
    Ideal para tiendas Retail, Food-Court, Restaurantes.
    El cliente elige los productos que desea adquirir o consumir en el establecimiento de su  preferencia,  dependiendo del modelo de negocio el pedido puede llegar a la bodega o al área de producción,  adicional la transacción de emisión de la factura se transmite a la caja.
    Este módulo viene incorporado en nuestro ERP,  si cuenta con un ERP propio, realizamos las adecuaciones necesarias para su integración.
    </p>
 </div>
   
<div class="cont">
    <h2 class="text-center title">CARACTERISTICAS PRINCIPALES</h2>
    <div class="" style="display: grid; grid-template-columns: 1fr .4fr;">
      <div>
        <p id="primera-kiosco" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Rápido y sencillo
        </p>
        <p id="segunda-kiosco" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Presentación en formato de fotos
        </p>
        <p id="tercera-kiosco" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Complementos y Modificadores
        </p>
        <p id="cuarta-kiosco" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Manejo de comentarios
        </p>

      </div>
      <!--Contenedor de video-->
      <video class="sombra" width="511px" height="287px" id="video-seleccionado" autoplay loop >
      <source src="https://danielgomez.website/micropos/wp-content/uploads/2019/06/VIDEO-KIOSCO-SELF-SERVICES.mp4" type="video/mp4">
      Your browser does not support the video tag.
      </video>
    </div>
</div>


<div class="cont gridy" style="padding: 5% 0;"> 
 
    <div class="" >
          <h2 class="blacky" style="margin: 0;">
            Características Detalladas
          </h2>
          <ul class="custom-style-type" style="padding: 0 10% 0 0;">
            <li>
            Múltiple compañía y sucursales
            </li>
            <li>Kiosco configurable para múltiples compañías</li>
            <li>Múltiple compañía y sucursales</li>
            <li>Múltiple bodegas</li>
            <li>Corte X</li>
            <li>Cierre fiscal</li>
            <li>Establezca las áreas de emisión de pedidos o bodegas.</li>
			  <li>Selección de artículos,  por referencia,  código de barra o imagen</li>
			  <li>Manejo de complemento o modificadores por grupos o productos</li>
            <li>Envio de los artículos al área de despacho</li>
			  <li>Permite colocar comentarios para el despacho de los artículos</li>
            <li>Rebaja los insumos de productos terminados con indicador de rebaja en la línea de ventas</li>
            <li>Configure las diferentes áreas de facturación</li>
			  <li>Permite cerrar transacciones con  varias formas de pagos</li>
          </ul>
      </div>
      <div style="">
        <div class="" style="width: 100%; height: 100%; background-image: url(<?php echo get_template_directory_uri(); ?>/img/pics/ddd.jpg);"></div>
      </div>
    </div>

<?php get_footer() ?>