<?php 

get_header();
$linkheader = "https://danielgomez.website/micropos/wp-content/uploads/2019/05/Banner_Retail.jpg";

?>


<div class="cont fixy">
    <img style="width: 100%;" src="<?php echo get_template_directory_uri(); ?>/img/pics/montaje-retail.jpg" alt="banner">
    <h2 class="title">MicroPos RETAIL & PYME, Punto De Venta</h2>
    <p class="d-flex" style="">
    Este modulo forma parte de una serie de aplicaciones que rodean nuestro ERP,  elPunto De Ventas esta diseñado para cualquier tipo de industria RETAIL & PYME,hacemos diseños personalizados si el cliente así lo solicita.     
    </p>
</div>

    <div class="cont" style="margin: 5% auto;">
    <h2 class="text-center title">CARACTERISTICAS PRINCIPALES</h2>
    <div class="gridy">
      <div>
        <p id="primera" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Manejo de
          Complementos o Modificadores
        </p>
        <p id="segunda" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Abono de mercancía
        </p>
        <p id="tercera" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Recepción de pagos
          de abonos
        </p>
        <p id="cuarta" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Pago mixtos 
        </p>
        <p id="quinta" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Venta De Certificado de regalos 
        </p>
        <p id="sexta" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Devolución de mercancías 
        </p>
      </div>
      <!--Contenedor de video-->
      <video class="sombra" width="511px" height="287px" id="video-seleccionado" autoplay loop >
      <source src="<?php echo get_template_directory_uri(); ?>/videos/primera.mp4" type="video/mp4">
      Your browser does not support the video tag.
      </video>
    </div>
  </div>

<div class="cont">
  <div class="gridy">
    <div class="" style="padding-bottom: 5%;">
          <h2 class="blacky" style="margin: 0;">
            Características Detalladas
          </h2>
          <ul class="custom-style-type" style="padding: 0 10% 0 0;">
            <li>
            Múltiple compañía y sucursales
            </li>
            <li>Múltiple bodegas</li>
            <li>Apertura  de múltiples cajas por día</li>
            <li>Depósito y retiros desde la caja</li>
            <li>Corte X</li>
            <li>Cierre por turno de cajeros</li>
            <li>Cierre fiscal</li>
            <li>Recepción  de pagos de CXC</li>
            <li>Ventas al contado o al crédito</li>
            <li>Ventas en espera</li>
            <li>Emisión de certificado de regalos</li>
			  <li>Cierre de pedidos o de  cuentas abiertas de estaciones de pedidos</li>
            <li>Procesamiento de abonos de mercancía</li>
			  <li>Selección de artículos,  por referencia,  código de barra o imagen</li>
			  <li>Manejo de complemento o modificadores por grupos o productos</li>
            <li>Envio de los artículos al área de despacho</li>
			  <li>Permite colocar comentarios para el despacho de los artículos</li>
			  <li>Rebaja los insumos de productos terminados con indicador de rebaja en la línea de ventas</li>
            <li>Emite asientos contables para cada emisión de transacciones
				</li>
            <li>Forma de pagos configurables por el usuario</li>
            <li>Permite cerrar transacciones con  varias formas
				de pagos</li>
            <li>Configuración para emisión de nota de entrega desde la estación de caja</li>
          </ul>
      </div>
      <div style="">
        <div class="cuadrado cover" style="margin-bottom: 5%; background: url(<?php echo get_template_directory_uri(); ?>/img/pics/aaa.jpg);"></div>
        <div class="cuadrado cover" style="margin: 5% 0; background: url(<?php echo get_template_directory_uri(); ?>/img/pics/bbb.jpg);"></div>
      </div>
    </div>
</div>

<?php get_footer() ?>