<?php 

get_header();
$linkheader = "https://danielgomez.website/micropos/wp-content/uploads/2019/05/Banner_Retail.jpg";

?>


<div class="cont fixy">
    <img style="width: 100%;" src="<?php echo get_template_directory_uri(); ?>/img/pics/montaje-02.jpg" alt="banner">
    <h2 class="title">MicroPos RESTAURANTES,  Punto de Ventas</h2>
    <p class="d-flex" style="">
    Este modulo forma parte de una serie de aplicaciones que rodean nuestro ERP, la estación de caja esta diseñada para cualquier tipo de industria de HOSPITALIDAD, hacemos diseños personalizados si el cliente así lo solicita.</div>

<div class="cont" style="margin: 5% auto;">
    <h2 class="text-center title">CARACTERISTICAS PRINCIPALES</h2>
    <div class="gridy">
      <div>
        <p id="primera-restaurantes" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Factura en tres pasos
        </p>
        <p id="segunda-restaurantes" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Complementos y modificadores
        </p>
        <p id="tercera-restaurantes" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Envio de comandas a áreas preestablecidas
        </p>
        <p id="cuarta-restaurantes" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Estación de Meseros
        </p>
        <p id="quinta-restaurantes" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Pagos Mixtos
        </p>

      </div>
      <!--Contenedor de video-->
      <video class="sombra" width="511px" height="287px" id="video-seleccionado" autoplay loop >
      <source src="<?php echo get_template_directory_uri(); ?>/videos/restaurantes/primera.mp4" type="video/mp4">
      Your browser does not support the video tag.
      </video>
    </div>
  </div>

<div class="cont">
    <div class="gridy">
      <div class="" style="padding-bottom: 5%;">
          <h2 class="blacky" style="margin: 0;">
            Características Detalladas
          </h2>
          <ul class="custom-style-type" style="padding: 0 10% 0 0;">

          <li>Múltiple compañía y sucursales.</li>

     <li>Múltiple bodegas.</li>

     <li>Apertura  de múltiples cajas por día.</li>

     <li>Depósito y retiros desde la caja.</li>

     <li>Corte X.</li>

     <li>Cierre por turno de cajeros.</li>

     <li>Cierre fiscal.</li>

     <li>Recepción  de pagos de CXC.</li>

     <li>Ventas al contado o al crédito.</li>

			  <li>Selección de artículos,  por referencia,  código de barra o imagen.</li>

			  <li> Manejo de complemento o modificadores por grupos o productos.</li>

			  <li>Permite colocar comentarios para el área de preparación del producto.</li>

			  <li>Rebaja los insumos de productos terminados con indicador de rebaja en la línea de ventas.</li>

			  <li>Emite asientos contables por cada emisión de transacción.</li>

     <li>Forma de pagos configurables por el usuario.</li>

			  <li>Permite cerrar transacciones con  varias formas de pagos</li>

     <li>Puede incorporar estaciones de meseros</li>
            
          </ul>
      </div>
      <div style="">
        <div class="cuadrado img-center" style="margin-bottom: 5%; background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/pics/restaurante-1.jpg);"></div>
        <div class="cuadrado img-center" style="margin: 5% 0; background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/pics/monatje-05.jpg"></div>
      </div>
    </div>

</div>


<?php get_footer() ?>