<?php get_header() ?>

<style>.block{display: block;}.btn-custom{display: inline-block !important}</style>

<div class="cont">
    <img style="100%" src="<?php echo get_template_directory_uri(); ?>/img/pics/montaje-01.jpg" alt="logo"/>
</div>

    
	<style>
		.gridoblock{
			display: grid;
			padding: 5% 0;
		}
		.hoverimg:hover{
			opacity: .7;
		}
		@media screen and (max-width: 600px){
			.gridoblock{
			display: block;
		}
		}
</style>
	<div class="cont gridoblock" style="grid-template-columns: 1fr 1fr 1fr 1fr;">
		<a class="hoverimg" href="#retail"><img src="<?php echo get_template_directory_uri(); ?>/img/logoretail.png" alt=""></a>
		<a class="hoverimg" href="#restaurante"><img src="<?php echo get_template_directory_uri(); ?>/img/logorestaurante.png" alt=""></a>
		<a class="hoverimg" href="#erp"><img src="<?php echo get_template_directory_uri(); ?>/img/logoerp.png" alt=""></a>
		<a class="hoverimg" href="#kiosco"><img src="<?php echo get_template_directory_uri(); ?>/img/logokiosco.png" alt=""></a>
	</div>

    <div style="padding-bottom: 10%;" id="retail">
      <div class="cont" >
        <h2 class="title blacky">
        MicroPos RETAIL & PYME, Punto De Venta
        </h2>
        <p style="padding-bottom: 5%;">
        Este modulo forma parte de una serie de aplicaciones que rodean nuestro ERP,  elPunto De Ventas esta diseñado para cualquier tipo de industria RETAIL & PYME,hacemos diseños personalizados si el cliente así lo solicita.     

        </p>
      </div>

      <div class="cont gridy">
        <div style="margin:auto">
          <h2 class="title blacky" style="margin: 0;">
			  Características <span style="margin-left: 54px;">Generales</span>
          </h2>
          <ul class="custom-style-type">
            <li>
            En tres pasos emita una factura
                        </li>
            <li>Selección de artículos,  por referencia,  código de barra o imagen</li>
			  <li>Permite cerrar transacciones con  varias formas de pagos</li>
            <li>Ventas al contado o al crédito</li>
            <li>Ventas en espera</li>
            <li>Manejo de Complementos o Modificadores</li>
            <li>Abono de mercancía</li>
            <li>Recepción de pagos de abonos</li>
            <li>Recepcion de pagos de cuentas por cobrar</li>
            <li>Emisión y  recepción de certificados de regalo</li>
     
          </ul>
          <a href="<?php echo esc_url( home_url('/micropos-retail-pyme') ); ?>" class="btn-custom block" style="margin-top: 24px;">Características Detalladas</a>
        </div>

        <div
          style="background: url(<?php echo get_template_directory_uri(); ?>/img/pics/montaje-retail.jpg) no-repeat center;
        background-size: cover; margin: 5%"
          class="wow fadeInUp"
        ></div>
      </div>
    </div>


<div style="padding-bottom: 2%;" id="restaurante">
      <div class="cont" >
        <h2 class="title blacky">
          MicroPos RESTAURANTES
        </h2>
        <p style="padding-bottom: 5%;">
    Este modulo forma parte de una serie de aplicaciones que rodean nuestro ERP, la estación de caja esta diseñada para cualquier tipo de industria de HOSPITALIDAD, hacemos diseños personalizados si el cliente así lo solicita.

        </p>
      </div>

      <div class="cont gridy">
        <div style="margin:auto">
         <h2 class="title blacky" style="margin: 0;">
			  Características <span style="margin-left: 54px;">Generales</span>
          </h2>
          <ul class="custom-style-type">
          <li>
            En tres pasos emita una factura 
                        </li>
            <li>Permite cerrar transacciones con varias formas de pagos</li>
            <li>Ventas en espera</li>
            <li>Recepcion de pagos de cuentas por cobrar</li>
            <li>Ventas a domicilio</li>
			 <li>Rebaja inventario de producto terminado o de recetas</li>
			  <li>Estaciones de Meseros y Self Sevices</li>
          </ul>
          <a href="<?php echo esc_url( home_url('/micropos-restaurantes') ); ?>" class="btn-custom block" style="margin-top: 24px;">Características Detalladas</a>
        </div>

        <div
          style="background: url(<?php echo get_template_directory_uri(); ?>/img/pics/montaje-02.jpg) no-repeat center;
          background-size: cover; background-position: top center; margin: 5%"
          class="wow fadeInUp"
        ></div>
      </div>
    </div>



    <div style="padding-bottom: 2%;" id="erp">
      <div class="cont" >
 
		<div class="servicios-cont-grid">
			        <div style="padding: 10% 5% 10% 0%;">
						       <h2 class="title blacky">
        MicroPos ERP
        </h2>
			<p>
        Nuestro ERP esta diseñado para trabajar en cualquier modelo de industria, desde una empresa que facture servicio, hasta una de fabricación de productos consumibles y no consumibles. MicroPos ERP, Te ayuda en la agilización de tus operaciones y en el servicio al cliente, genera información en tiempo real para la toma de decisiones, un sistema escalable que se adapta al crecimiento de tu negocio.    
			</p>
			        <a href="<?php echo esc_url( home_url('/micropos-erp') ); ?>" class="btn-custom block" style="margin-top: 24px;">Características Detalladas</a>
			</div>
			   <div
          style="background: url(<?php echo get_template_directory_uri(); ?>/img/pics/montaje-03.jpg) no-repeat center;
          background-size: cover; background-position: top center; margin: 5%; min-height: 400px"
          class="wow fadeInUp"
        ></div>
		</div>
      </div>

      <!-- <div class="cont gridy">
        <div style="">
        <h2 class="blacky" style="margin: 0;">
          Tipo de Industrias
          </h2>
          <ul class="custom-style-type">
            <li>Tiendas Retail & Hospitalidad</li>
            <li>Distribuidora mayoristas o minoristas</li>
            <li>Súper Mercados</li>
            <li>Tiendas por Departamentos</li>
            <li>Sala de Belleza ,  Barberías y SPA</li>
            <li>Auto Repuesto y Ferretería</li>
            <li>Centro de Copiados</li>
            <li>Auto repuestos y Ferreterías</li>
            <li>Restaurantes ,  Bares, Refresquería y Similares</li>
            <li>Productoras de alimentos</li>
            <li>Devolución de artículos</li>
          </ul>
          <a href="/micropos-erp" class="btn-custom block">Características Detalladas</a>
        </div>
        <div
          style="background:/img/pics/montaje-03.jpg) no-repeat center;
    background-size: cover; margin: 5%"
          class="wow fadeInUp"
        ></div>
      </div>
 -->    </div>



    <div style="padding-bottom: 10%;" id="kiosco">
      <div class="cont" >
        <h2 class="title blacky">
          Kiosco Self Services
        </h2>
        <p style="padding-bottom: 5%;">
        Nuestro kiosco es adaptable para cualquier tipo de  negocio que realice ventasde productos consumible o no consumible

        </p>
      </div>

      <div class="cont gridy">
        <div style="margin:auto">
          <h2 class="title blacky" style="margin: 0;">
			  Características <span style="margin-left: 54px;">Generales</span>
          </h2>
          <ul class="custom-style-type">
          <li>
            Kiosco configurable para múltiples compañías
                        </li>
            <li>Presente los grupos de ventas y artículos en formato de fotos</li>
            <li>Establezca las áreas de emisión de pedidos o bodegas</li>
            <li>Toda las características ampliadas de nuestro Sistema de Puntos Ventas</li>
            <li>Configure las diferentes áreas de facturación</li>
          </ul>
          <a href="<?php echo esc_url( home_url('/micropos-kiosco-self-service') ); ?>" class="btn-custom block" style="margin-top: 24px;">Características Detalladas</a>
        </div>

        <div
          style="background: url(<?php echo get_template_directory_uri(); ?>/img/pics/montaje5.jpg) no-repeat center;
          background-size: cover; margin: 5%"
          class="wow fadeInUp"
        ></div>
      </div>
    </div>

<?php get_footer() ?>