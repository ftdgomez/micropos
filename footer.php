<?php get_template_part('clientes') ?>
<footer class="footer">
        <div>
          <h3>Acerca De Nosotros</h3>
          <p>
          MicroPos Software, servicios tecnológicos y asesoría en negocio.
          </p>
		<a style="
				  padding: .5em 1.5em;
				  border: .2px solid white;
				  display: inline-block;
				 margin-top: .5em;
				  
				  " href="<?php echo esc_url( home_url('/contacto') ); ?>">Dirección ></a>
			
        </div>
        <div>
          <h3>Productos</h3>
			<a href="<?php echo esc_url( home_url('/micropos-erp') ); ?>">MicroPos ERP</a><br>
			<a href="<?php echo esc_url( home_url('/micropos-retail-pyme') ); ?>">MicroPos Retail & Pyme</a><br>
			<a href="<?php echo esc_url( home_url('/micropos-restaurantes') ); ?>">MicroPos Restaurantes</a><br>
          
          
          <a href="<?php echo esc_url( home_url('/micropos-kiosco-self-service') ); ?>">MicroPos Kiosco Self Services</a><br>
			          
        </div>
        <div>
          <h3>Servicios</h3>
          <a href="<?php echo esc_url( home_url('/tercerizacion-it') ); ?>">Tercerización IT</a><br>
          <a href="<?php echo esc_url( home_url('/desarrollo-a-la-medida') ); ?>">Desarrollo a la Medida</a><br>
          <a href="<?php echo esc_url( home_url('/asesoria-it') ); ?>">Asesoría IT</a><br>
        </div>
        <div>
          <h3><a href="<?php echo esc_url( home_url('/blog') ); ?>">Blog</a></h3>
          <h3 style="margin-bottom: 14px;">Síguenos
      <span class="d-flex" style="font-size: 14px;">
            <a class="social-logo" href="https://www.instagram.com/micropossof/"><img style="margin-left: 0;display: block;"
            src="<?php echo get_template_directory_uri(); ?>/img/icons8_instagram_new_96px.png"
            alt="instagram" /></a>
        <a class="social-logo" href="https://twitter.com/micropossof"><img style="display: block;"
            src="<?php echo get_template_directory_uri(); ?>/img/icons8_twitter_96px.png"
            alt="twitter" /></a>
        <a class="social-logo" href="https://www.facebook.com/micropossof"><img style="display: block;"
            src="<?php echo get_template_directory_uri(); ?>/img/icons8_facebook_96px.png"
            alt="facebook" /></a>
      </span>
          </h3>
			     <span class="d-flex" style="font-size: 14px; margin-bottom: 8px;">
        <img style="margin-right: 8px;"
          src="<?php echo get_template_directory_uri(); ?>/img/icons8_phonelink_ring_96px_1.png"
          alt="teléfono" />
        (507) 306-3700, Ext. 3606</span>
      <span class="d-flex" style="font-size: 14px;">
        <img style="margin-right: 8px;"
          src="<?php echo get_template_directory_uri(); ?>/img/icons8_mailing_96px.png" alt="mail:info@micropossoft.com" />
        info@micropossoft.com</span>

        </div>
  
      </footer>
         
      
		</div>
		<!-- /wrapper -->
		<?php wp_footer(); ?>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

	</body>
</html>