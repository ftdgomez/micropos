<?php get_header(); ?>

    <h2 class="text-center title boldy">Todos nuestros clientes dicen:</h2>

    <div class="clientes-cont cont">
      <div class="slide-clientes-page">
        <div class="clientes-container">
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/0.png); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/1.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/2.png); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/3.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/4.png); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/5.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/6.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/7.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
          <div
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/8.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
            alt=""
            class="logocliente"
          ></div>
        </div>
      </div>
      <div class="d-flex" style="padding: 10%;">
        <p>
          <i class="fas fa-quote-left"></i>
          <i>
            Mollis quam tellus nullam ridiculus hac aliquam velit metus augue,
            pellentesque mus aliquet condimentum penatibus hendrerit facilisi
            nostra risus, dui id morbi viverra taciti vitae interdum quisque.
            Aenean sapien himenaeos vel elementum magnis enim suscipit, urna
            curae ac ornare quisque imperdiet accumsan eleifend, vestibulum erat
            aliquam cras tristique eu.<span style="font-size: 2em">"</span>
          </i>
          <br />
          <span style="font-weight: 700; font-size: 1.5em;">Lorem Ipsum.</span
          ><br />
          <span>Cargo En Su Empresa</span>
        </p>
      </div>
    </div>
    <br />

<?php get_footer(); ?>