  <h2 class="text-center title">ALGUNOS DE NUESTROS CLIENTES
  </h2>
  <div class="text-center">
    <p class="text-center btn-especial-cont">
      <!-- <a href="<?php // echo esc_url( home_url('/clientes') ); ?>" style="text-decoration: none;">Ver Más</a> -->
    </p>

  </div>
    <div class="slider-clientes" style="width: 90%; margin: auto;">
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/0.png); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/1.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/2.png); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/3.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/4.png); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/5.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/6.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/7.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/8.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/9.jpg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/10.jpeg); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
      <div
        style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/cliente/11.png); height: 150px; width: 150px; background-size: contain; filter:grayscale(1); background-repeat: no-repeat; background-position: center;"
        alt="" class="logocliente"></div>
  </div>