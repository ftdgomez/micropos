<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<article style="margin: 5% 0;" id="post-<?php the_ID(); ?>" class="entry-content">
				
		<div class="entry-img" style="background-image: url(<?php the_post_thumbnail_url()?>); background-size: cover;"></div>
		<div class="entry-cont">
			<section class="entry-data">
				<a class="entry-title" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
				<p><?php echo excerpt('40'); ?></p>
				<a href="<?php the_permalink(); ?>"  class="btn-custom ">
				Leer Más</a>
			</section>
		</div>
				
	</article>

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'micropos' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
