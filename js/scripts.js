(function($, root, undefined) {
  $(function() {
    "use strict";

    // DOM ready, take it away

    //append menu img

    //$(".sub-menu").append("<div class='img-sub-menu'></div>");
    //$(".sub-menu").on("click", function() {
    //  showCoords(event);
    //});
    /*
    function showCoords(event) {
      var localhost = ""; //window.location.hostname;
      var x = event.clientX;
      var y = event.clientY;
      var coords = "X= " + x + ", Y= " + y;
      console.log(coords);
      if (x > 600 && x < 878 && y < 336) {
        localhost += "/micropos/micropos-erp/";
        window.location.assign(localhost);
      } else if (x > 600 && x > 878 && y < 336) {
        localhost += "/micropos/micropos-kiosco-self-service/";
        window.location.assign(localhost);
      } else if (x > 600 && x < 878 && y > 336) {
        localhost += "/micropos/micropos-restaurantes/";
        window.location.assign(localhost);
      } else if (x > 600 && x > 878 && y > 336) {
        localhost += "/micropos/micropos-retail-pyme/";
        window.location.assign(localhost);
      } else if (x < 600) {
        console.log("AQUI NO");
      }
    }*/

    //on hover change the img-sub-menu
    /*     $("#menu-item-42").on("hover", function() {
      $(".img-sub-menu").css("background", "red");
    });
    $("#menu-item-43").on("hover", function() {
      $(".img-sub-menu").css("background", "blue");
    });
    $("#menu-item-44").on("hover", function() {
      $(".img-sub-menu").css("background", "green");
    });
    $("#menu-item-45").on("hover", function() {
      $(".img-sub-menu").css("background", "red");
    });
    $("#menu-item-46").on("hover", function() {
      $(".img-sub-menu").css("background", "blue");
    });
    $("#menu-item-47").on("hover", function() {
      $(".img-sub-menu").css("background", "green");
    });
 */
    // zoom

    if (window.innerWidth > 1024) {
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        $(".zoom").css({
          backgroundSize: 100 + scroll / 5 + "%",
          top: -(scroll / 10) + "%"
        });
      });
    }

    //video

    var vid = document.getElementById("video-seleccionado");

    function video1() {
      vid.src = "/wp-content/themes/micropos/videos/primera.mp4";
      vid.load();
      vid.play();
    }

    function video2() {
      vid.src = "/wp-content/themes/micropos/videos/segunda.mp4";
      vid.load();
      vid.play();
    }
    function video3() {
      vid.src = "/wp-content/themes/micropos/videos/tercera.mp4";
      vid.load();
      vid.play();
    }
    function video4() {
      vid.src = "/wp-content/themes/micropos/videos/cuarta.mp4";
      vid.load();
      vid.play();
    }
    function video5() {
      vid.src = "/wp-content/themes/micropos/videos/quinta.mp4";
      vid.load();
      vid.play();
    }
    function video6() {
      vid.src = "/wp-content/themes/micropos/videos/sexta.mp4";
      vid.load();
      vid.play();
    }

    function video1restaurantes() {
      vid.src = "/wp-content/themes/micropos/videos/restaurantes/primera.mp4";
      vid.load();
      vid.play();
    }

    function video2restaurantes() {
      vid.src = "/wp-content/themes/micropos/videos/restaurantes/segunda.mp4";
      vid.load();
      vid.play();
    }
    function video3restaurantes() {
      vid.src = "/wp-content/themes/micropos/videos/restaurantes/tercera.mp4";
      vid.load();
      vid.play();
    }
    function video4restaurantes() {
      vid.src = "/wp-content/themes/micropos/videos/restaurantes/cuarta.mp4";
      vid.load();
      vid.play();
    }
    function video5restaurantes() {
      vid.src = "/wp-content/themes/micropos/videos/restaurantes/quinta.mp4";
      vid.load();
      vid.play();
    }
    function video6restaurantes() {
      vid.src = "/wp-content/themes/micropos/videos/restaurantes/sexta.mp4";
      vid.load();
      vid.play();
    }

    function video1erp() {
      vid.src = "/wp-content/themes/micropos/videos/erp/primera.mp4";
      vid.load();
      vid.play();
    }

    function video2erp() {
      vid.src = "/wp-content/themes/micropos/videos/erp/segunda.mp4";
      vid.load();
      vid.play();
    }
    function video3erp() {
      vid.src = "/wp-content/themes/micropos/videos/erp/tercera.mp4";
      vid.load();
      vid.play();
    }
    function video4erp() {
      vid.src = "/wp-content/themes/micropos/videos/erp/cuarta.mp4";
      vid.load();
      vid.play();
    }
    function video5erp() {
      vid.src = "/wp-content/themes/micropos/videos/erp/quinta.mp4";
      vid.load();
      vid.play();
    }
    function video6erp() {
      vid.src = "/wp-content/themes/micropos/videos/erp/sexta.mp4";
      vid.load();
      vid.play();
    }

    function video1kiosco() {
      vid.src = "/wp-content/uploads/2019/06/VIDEO-KIOSCO-SELF-SERVICES.mp4";
      vid.load();
      vid.play();
    }

    function video2kiosco() {
      vid.src =
        "/wp-content/uploads/2019/06/VIDEO-KIOSCO-SELF-SERVICES-IMAGENES.mp4";
      vid.load();
      vid.play();
    }
    function video3kiosco() {
      vid.src =
        "/wp-content/uploads/2019/06/VIDEO-KIOSCO-SELF-SERVICES-COMPLEMENTOS.mp4";
      vid.load();
      vid.play();
    }
    function video4kiosco() {
      vid.src =
        "/wp-content/uploads/2019/06/VIDEO-KIOSCO-SELF-SERVICES-COMENTARIOS.mp4";
      vid.load();
      vid.play();
    }
    //endvideo

    //video handlers
    $("#primera").on("click", function() {
      video1();
    });
    $("#segunda").on("click", function() {
      video2();
    });
    $("#tercera").on("click", function() {
      video3();
    });
    $("#cuarta").on("click", function() {
      video4();
    });
    $("#quinta").on("click", function() {
      video5();
    });
    $("#sexta").on("click", function() {
      video6();
    });

    //video handler restaurantes
    $("#primera-restaurantes").on("click", function() {
      video1restaurantes();
    });
    $("#segunda-restaurantes").on("click", function() {
      video2restaurantes();
    });
    $("#tercera-restaurantes").on("click", function() {
      video3restaurantes();
    });
    $("#cuarta-restaurantes").on("click", function() {
      video4restaurantes();
    });
    $("#quinta-restaurantes").on("click", function() {
      video5restaurantes();
    });
    $("#sexta-restaurantes").on("click", function() {
      video6restaurantes();
    });

    //video handler erp
    $("#primera-erp").on("click", function() {
      video1erp();
    });
    $("#segunda-erp").on("click", function() {
      video2erp();
    });
    $("#tercera-erp").on("click", function() {
      video3erp();
    });
    $("#cuarta-erp").on("click", function() {
      video4erp();
    });
    $("#quinta-erp").on("click", function() {
      video5erp();
    });
    $("#sexta-erp").on("click", function() {
      video6erp();
    });

    //video handler kiosco

    $("#primera-kiosco").on("click", function() {
      video1kiosco();
    });
    $("#segunda-kiosco").on("click", function() {
      video2kiosco();
    });
    $("#tercera-kiosco").on("click", function() {
      video3kiosco();
    });
    $("#cuarta-kiosco").on("click", function() {
      video4kiosco();
    });

    //$("#quinta-kiosco").on("click", function() {
    //  video5erp();
    //});
    //$("#sexta-kiosco").on("click", function() {
    //  video6erp();
    //});

    //open menu
    let open = true;
    $(".menu-ham").on("click", function() {
      if (open) {
        $(".custom-navigation-m").css("top", "0");
        open = false;
      } else {
        $(".custom-navigation-m").css("top", "-200%");
        open = true;
      }
    });

    //slider
    if (window.innerWidth > 1024) {
      $(".slider-clientes").slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        nextArrow: '<button type="button" class="slick-next">></button>',
        prevArrow: '<button type="button" class="slick-next"><</button>'
      });

      new WOW().init();
    } else if (window.innerWidth <= 1024 || window.innerWidth > 500) {
      $(".slider-clientes").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        nextArrow: '<button type="button" class="slick-next">></button>',
        prevArrow: '<button type="button" class="slick-next"><</button>'
      });

      new WOW().init();
    } else if (window.innerWidth < 500) {
      $(".slider-clientes").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: true,
        nextArrow: '<button type="button" class="slick-next">></button>',
        prevArrow: '<button type="button" class="slick-next"><</button>'
      });

      new WOW().init();
    }
  });
})(jQuery, this);
