<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	/**
	 * Fires in the head, before {@see wp_head()} is called. This action can be used to
	 * insert elements into the beginning of the head before any styles or scripts.
	 *
	 * @since 1.0
	 */
	do_action( 'et_head_meta' );

	$template_directory_uri = get_template_directory_uri();
?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="header-cont">
            <header class="main-header-custom cont">
                    <img class="logo-placeholder" src="https://danielgomez.website/micropos/wp-content/uploads/2019/04/MicroPos_Logo_FondoTransp-01.png" alt="logo">
                    <nav class="custom-navigation">
                        <a class="item-header-custom" href="#">Productos</a>
                        <a class="item-header-custom" href="#">Servicios</a>
                        <a class="item-header-custom" href="#">Clientes</a>
                        <a class="item-header-custom" href="#">Contacto</a>
                        <a class="btn-custom" href="#">Ver Catálogo</a>
                    </nav>
                </header>
            </div>
                <div class="cont">
                    <div class="second-bar-custom d-flex">
                        <span class="d-flex" style="font-size: 14px;">
                            <img src="https://danielgomez.website/micropos/wp-content/uploads/2019/04/icons8_phonelink_ring_96px_1.png" alt="teléfono">
                            (507) 306-3700, Ext. 3606</span>
                        <span class="d-flex" style="font-size: 14px;">
                            <img src="https://danielgomez.website/micropos/wp-content/uploads/2019/04/icons8_mailing_96px.png" alt="mail:">
                            contacto@micropossoft.net</span>
                        <span class="d-flex" style="font-size: 14px;">
                            <a href="#"><img style="margin-left: 1em;display: block;" src="https://danielgomez.website/micropos/wp-content/uploads/2019/04/icons8_instagram_new_96px.png" alt="instagram"></a>
                            <a href="#"><img style="margin-left: 1em;display: block;" src="https://danielgomez.website/micropos/wp-content/uploads/2019/04/icons8_twitter_96px.png" alt="twitter"></a>
                            <a href="#"><img style="margin-left: 1em;display: block;" src="https://danielgomez.website/micropos/wp-content/uploads/2019/04/icons8_facebook_96px.png" alt="facebook"></a>
                        </span>
                    </div>
                </div>
            