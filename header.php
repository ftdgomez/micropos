<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,700" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <?php wp_head(); ?>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css" />
</head>

	<body <?php body_class(); ?>>
 <div style="height: 100px;"></div>
		<!-- wrapper -->
<div class="">
  <!--====================================================================
    sheader
    ========================================================================-->
	<div class="header-cont nav-down">
    <header class="main-header-custom cont">
      <a href="<?php echo esc_url( home_url('/') ); ?>"><img class="logo-placeholder"
          src="<?php echo get_template_directory_uri(); ?>/img/main-logo.png"
          alt="logo" />
      </a>
      
      <nav class="custom-navigation">
        <?php micropos_nav(); ?>
      </nav>

      <div class="menu-ham"><i class="fas fa-bars"></i></div>
      <nav class="custom-navigation-m">
        <?php micropos_nav(); ?>
      </nav>
    </header>
  </div>
  <!--====================================================================
    subheader
    ========================================================================-->
  <div class="cont" style="position:relative;z-index:2;">
    <div class="second-bar-custom d-flex">
      <span class="d-flex" style="font-size: 14px;">
        <img style="margin-right: 8px;"
          src="<?php echo get_template_directory_uri(); ?>/img/icons8_phonelink_ring_96px_1.png"
          alt="teléfono" />
        (507) 306-3700, Ext. 3606</span>
      <span class="d-flex" style="font-size: 14px;">
        <img style="margin-right: 8px;"
          src="<?php echo get_template_directory_uri(); ?>/img/icons8_mailing_96px.png" alt="mail:info@micropossoft.com" />
        info@micropossoft.com</span>
      <span class="d-flex" style="font-size: 14px;">
        <a class="social-logo" href="https://www.instagram.com/micropossof/"><img style="margin-left: 1em;display: block;"
            src="<?php echo get_template_directory_uri(); ?>/img/icons8_instagram_new_96px.png"
            alt="instagram" /></a>
        <a class="social-logo" href="https://twitter.com/micropossof"><img style="margin-left: 1em;display: block;"
            src="<?php echo get_template_directory_uri(); ?>/img/icons8_twitter_96px.png"
            alt="twitter" /></a>
        <a class="social-logo" href="https://www.facebook.com/micropossof"><img style="margin-left: 1em;display: block;"
            src="<?php echo get_template_directory_uri(); ?>/img/icons8_facebook_96px.png"
            alt="facebook" /></a>
      </span>
    </div>
  </div>
