<?php get_header() ?>
<style>
    .hero-img {
      height: calc(100vh - 60px);
      background-image: url(https://danielgomez.website/micropos/wp-content/themes/micropos/img/hero.jpg);
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
      background-attachment: fixed;
      margin-top: -50px;
      display: flex;
      align-items: flex-end;
      transition: all .1s linear;
    }
  </style>
    <div class="hero-img">
    
    </div>

    <div class="servicios-cont">
      <h2 class="title boldy">Tercerización IT</h2>

      <h2>Metodología de trabajo</h2>
      <p>
        MicroPos Software asignará un Analista/Programador con experiencia en
        las plataformas solicitadas por el cliente, éste recurso trabaja
        estrechamente con la Gerencia de Desarrollo para implementar los
        requerimientos solicitados en el espacio de tiempo estimado por la
        institución y cuyas actividades principales serán:
      </p>

      <div class="servicios-cont-grid">
        <div
          style="background: url(<?php echo get_template_directory_uri(); ?>/img/02.jpg); background-size: cover;background-position: center;"
        ></div>
        <div class="servicios-item">
          <ul class="custom-style-type">
            <li>
              Trabajar estrechamente con la Gerencia de Desarrollo o Jefe de
				Desarrollo asignado.
            </li>
            <li>
              Seguir los lineamientos en cuanto a estándares 
				UX & UI, definidos
				por la Gerencia de Desarrollo.
            </li>
            <li>
              Participar en cualquier reunión necesaria que 
				ayude a comprender
				los requerimientos solicitados.
            </li>
            <li>
              Implementar los requerimientos de acuerdo a orden de prioridad
				que asigne el cliente.
            </li>
			             <li>
              Convertir los requerimientos en códigos 
				secuenciales que sean
				ejecutados en la plataforma establecida.
            </li>
            <li>
              En fase de avances presentar pruebas unitarias al 
				área de negocio
				y operaciones.
            </li>
            <li>Participar en el proceso de pruebas y calidad.</li>
            <li>
              Hacer entrega de los fuentes y ejecutables una vez se certifique
				por las áreas de negocios y operaciones.
            </li>
          </ul>
        </div>
      </div>
				  	          <p class="">
          En base al tiempo estimado para la implementación de los
          requerimientos, nuestros recursos estarán entrenados para hacer uso de
          la metodología ágil XP o la que haga uso en su momento el CLIENTE.
        </p>

    </div>
    <div class="" style="padding: 3% 0">
      <div class="cont">
        
		  <div class="servicios-cont-grid">
			  <div style="padding: 10% 5% 10% 0%;">	 
				  <h2 class="title">Desarrollo a la Medida</h2>
				<p style="text-align: left;">
				  A través del uso de metodologías ágiles, nuestro equipo trabaja en
				  conjunto con el cliente para sacar soluciones tecnológicas que estén
				  alineadas a la estrategia del negocio, esto haciendo uso de las
				  mejores herramientas de Desarrollo De Software.
				</p>
				<a href="#">Desarrollo en plataformas winform, web, AS400</a>
			  </div>
		  
			<div style="background: url(<?php echo get_template_directory_uri(); ?>/img/01.jpg); background-size: cover;background-position: center;"></div>
			  
		  </div>
      </div>
    </div>

    <div class="cont" style="padding-bottom: 10%;">
      <div class="">
        <h2 class="title" style="margin-bottom: 0 !important">Asesoría IT</h2>
        <p>
          Asesoría en elaboración e implementación de Metodologías Agiles de
          Desarrollo de Software, Ingeniería de Software y Administración de
          Proyectos.
        </p>
        <a href="#">Desarrollo en plataformas winform, web, AS400</a>
      </div>
		
      <br />
		<div class="servicios-cont-grid">
			      <div class="servicios-item">
        <h2>Descripción</h2>
        <ul class="custom-style-type">
          <li>
            Metodologías de Desarrollo de Software, (Programación Extrema XP).
          </li>
          <li>
            Ingeniería de Software.
          </li>
          <li>
            Uso adecuado de recurso humano para el desarrollo de Software.
          </li>
          <li>
            Desarrollo a la medida (Jquery, Arquitectura MVC, Ajax, winform,
            winnet, PDA, Mobile).
          </li>
          <li>
            Herramientas para el Desarrollo Acelerado de Software, como
            (Infragistics, Crystal Report).
          </li>
        </ul>
      </div>
				
		     <div
          style="background: url(<?php echo get_template_directory_uri(); ?>/img/03.jpg); background-size: cover;background-position: center;"
        ></div>
		</div>

    </div>


<?php get_footer() ?>