<?php get_header() ?>
<style>
    .hero-img {
      height: calc(100vh - 60px);
      background-image: url(https://danielgomez.website/micropos/wp-content/themes/micropos/img/hero.jpg);
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
      background-attachment: fixed;
      margin-top: -50px;
      display: flex;
      align-items: flex-end;
      transition: all .1s linear;
    }
  </style>
    <div class="hero-img">
    
    </div>

    <div style="padding: 5% 0;">
      <div class="cont text-center">
        <h2>Perfil De La Empresa</h2>
        <p>
          MicroPos Software, empresa panameña que inicia operaciones en Abril
          del 2008, se dedica a ofrecer servicios tecnológicos y asesoría en
          negocio, nos especializamos en Sistemas de Punto de Ventas y
          Desarrollo de Software a la medida.
        </p>
      </div>

      <div class="quienes-somos-cont">
        <div class="">
          <i class="far fa-eye"></i>
          <h2 class="boldy">VISIÓN</h2>
          <p>
            Ser reconocidos por la calidad de soluciones tecnológicas y
            servicios que ofrecemos en los mercados en que incursionemos local o
            internacionalmente.
          </p>
        </div>

        <div class="">
          <i class="fas fa-heart"></i>
          <h2 class="boldy">VALORES</h2>
          <ul class="valores-ul" style="padding-bottom: 1em;">
            <li>> Responsabilidad</li>
            <li>> Pasión</li>
            <li>> Honestidad</li>
            <li>> Calidad</li>
            <li>> Innovación</li>
            <li>> Compromiso</li>
            <li>> Profesionalismo</li>
            <li>> Trabajo en equipo</li>
          </ul>
        </div>
      </div>
      <div class="cont">
        <div>
          <i class="fas fa-tasks"></i>
          <h2 class="boldy">MISIÓN</h2>
        </div>
        <p>
          Atraer, Satisfacer y Retener clientes, distribuidores y socios
          comerciales ofreciéndoles soluciones tecnológicas y servicios de
          outsourcing con los más altos estándares de la industria y de forma
          oportuna. Contratar, retener y capacitar el mejor talento humano que
          nos ayude a satisfacer y a superar las expectativas de nuestros
          clientes y socios comerciales.
        </p>
      </div>
    </div>
    <?php get_footer() ?>