<?php 

get_header();
$linkheader = "https://danielgomez.website/micropos/wp-content/uploads/2019/05/Banner_Retail.jpg";

?>


<div class="cont fixy">
    <img style="width: 100%;" src="<?php echo get_template_directory_uri(); ?>/img/pics/montaje-03.jpg" alt="banner">
    <h2 class="title">MicroPos ERP</h2>
    <p class="d-flex" style="">
    Nuestro ERP esta diseñado para trabajar en cualquier modelo de industria,  desde una empresa que facture servicio,  hasta una  de fabricación de productos consumibles y no consumibles.
<br><br>
MicroPos ERP,  Te ayuda en la agilización de tus operaciones y en el servicio al cliente,  genera información en tiempo real para la toma de decisiones, un sistema escalable que se adapta al crecimiento de tu negocio.  
</div>


<div class="cont" style="margin: 5% auto;">
    <h2 class="text-center title">CARACTERISTICAS PRINCIPALES</h2>
    <div class="gridy">
      <div>
        <p id="primera-erp" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Múltiples compañías y sucursales
        </p>
        <p id="segunda-erp" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Manejo de múltiple catálogos
        </p>
        <p id="tercera-erp" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Ordenes de compras
        </p>
        <p id="cuarta-erp" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Recepción de Notas De Entrega
        </p>
        <p id="quinta-erp" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Orden de producción
        </p>
        <p id="sexta-erp" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Recepción de Pago de Facturas
        </p>

      </div>
      <!--Contenedor de video-->
      <video class="sombra" width="511px" height="287px" id="video-seleccionado" autoplay loop >
      <source src="<?php echo get_template_directory_uri(); ?>/videos/erp/primera.mp4" type="video/mp4">
      Your browser does not support the video tag.
      </video>
    </div>
  </div>


   

    <div style="margin: 5% 0; background: gray;"></div>
    <div class="cont gridy" style="padding-bottom: 5%;">
    <div class="" >
          <h2 class="blacky" style="margin: 0;">
            Características Detalladas
          </h2>
          <p><b>Ideal para  Industrias como:</b></p>
          <ul class="custom-style-type" style="padding: 0 10% 0 0;">
        
            <li>Tiendas Retail & Hospitalidad</li>
            <li>Distribuidora mayoristas o minoristas</li>
            <li>Súper Mercados</li>
            <li>Tiendas por Departamentos</li>
            <li>Sala de Belleza, Barberías y SPA</li>
          
            <li>Centro de Copiados</li>
            <li>Auto repuestos y Ferreterías</li>
            <li>Bares, Restaurantes y Similares</li>
            <li>Productoras de alimentos</li>
          </ul>
      </div>
      <div style="">
        <div class="" style="width: 100%; height: 100%; background: url(<?php echo get_template_directory_uri(); ?>/img/pics/7.jpg);background-size: cover;background-position: center;"></div>
      </div>
    </div>
</div>

<?php get_footer() ?>