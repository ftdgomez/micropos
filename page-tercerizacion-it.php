<?php 

get_header();
$linkheader = "https://danielgomez.website/micropos/wp-content/uploads/2019/05/Banner_Retail.jpg";

?>


<div class="cont fixy">
    <img style="width: 100%;" src="<?php echo get_template_directory_uri(); ?>/img/02.jpg" alt="banner">
    <h2 class="title">Tercerización IT</h2>
    <div class="" >
        <div class="" style="">
          <h2 class="blacky" style="margin: 0;">
          Metodología de trabajo
          </h2>
          <p class="d-flex" style="">
          MicroPos Software asignará un Analista/Programador con experiencia en las

            plataformas solicitadas por el cliente, éste recurso trabaja estrechamente con la

            Gerencia de Desarrollo para implementar los requerimientos solicitados en el

            espacio de tiempo estimado por la institución y cuyas actividades principales serán:
        </p>
			<style>.ocho-puntos li{padding: 8px 0}; .ocho-puntos{margin: 24px 0};</style>
          <ol class="custom-style-type ocho-puntos">
            <li>Trabajar estrechamente con la Gerencia de Desarrollo o Jefe de Desarrollo asignado.</li>
            <li>Seguir los lineamientos en cuanto a estándares UX & UI, definidos por la Gerencia

de Desarrollo</li>
            <li>Participar en cualquier reunión necesaria que ayude a comprender los requerimientos

solicitados</li>
            <li>Implementar los requerimientos de acuerdo al orden de prioridad que asigne el cliente</li>
            <li>Convertir los requerimientos en códigos secuenciales que sean ejecutados en la

plataforma establecida</li>
<li>En fase de avances presentar pruebas unitarias al área de negocio y operaciones.</li>
<li>Participar en el proceso de pruebas y calidad</li>
<li>Hacer entrega de los fuentes y ejecutables una vez se certifique por las áreas de

de negocios y operaciones</li>
          </ol>
          <p>
          En base al tiempo estimado para la implementación de los requerimientos, nuestros

recursos estarán entrenados para hacer uso de la metodología ágil XP o la que haga

uso en su momento el CLIENTE.          </p>
      </div>
    </div>

</div>
<div style="padding: 5%"></div>

<?php get_footer() ?>