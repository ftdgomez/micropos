<?php get_header() ?>
<style>
    .hero-img {
      height: calc(100vh - 60px);
      background-image: url(<?php echo get_template_directory_uri(); ?>/img/hero.jpg);
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
      background-attachment: fixed;
      margin-top: -50px;
      display: flex;
      align-items: flex-end;
      transition: all .1s linear;
    }
  </style>
  <!--====================================================================
    Slider
    ======================================================================== -->
    <div class="hero-img"></div>
 
  <!--====================================================================
    caracteristicas
    ========================================================================-->
  <div class="cont m-out" style="margin-top: 5%;">
    <h2 class="text-center">Punto De Venta Retail & Pyme</h2>
	  <h2 class="text-center title">CARACTERISTICAS PRINCIPALES</h2>
    <div class="gridy">
    <div>
        <p id="primera" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Manejo de
          Complementos o Modificadores
        </p>
        <p id="segunda" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Abono de mercancía
        </p>
        <p id="tercera" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Recepción de pagos
          de abonos
        </p>
        <p id="cuarta" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Pago mixtos 
        </p>
        <p id="quinta" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" /> Venta De Certificado de regalos 
        </p>
        <p id="sexta" class="play-cont d-flex">
          <img src="<?php echo get_template_directory_uri(); ?>/img/play-black.png" alt="play btn" />Devolución de mercancías 
        </p>
      </div>
      <!--Contenedor de video-->
      <video class="sombra" width="511px" height="287px" id="video-seleccionado" autoplay loop >
      <source src="<?php echo get_template_directory_uri(); ?>/videos/primera.mp4" type="video/mp4">
      Your browser does not support the video tag.
      </video>
    </div>
  </div>
  <div class="m-out" style="margin: 5% auto;">
    <p class="text-center btn-especial-cont">
      <a href="<?php echo esc_url( home_url('/productos') ); ?>"><span class="btn-especial">Ver Todas Las Caracteristicas</span></a>
    </p>
  </div>
  <section class="gridy">
    <div class="paddy bg-gris">
      <h2>Tercerización IT</h2>
      <p>
        MicroPos Software asignará un Analista/Programador con experiencia en
        las plataformas solicitadas por el cliente, éste recurso trabaja
        estrechamente con la Gerencia de Desarrollo para implementar los
        requerimientos solicitados en el espacio de tiempo estimado por la
        institución y cuyas actividades principales serán.
      </p>
      <a class="btn-custom" href="<?php echo esc_url( home_url('/tercerizacion-it') ); ?>">Leer Más</a>
    </div>
    <div class="wow fadeInRight" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/02.jpg);background-size: cover;"></div>
    <div class="grid-img-bg wow fadeInLeft" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/01.jpg);background-size: cover;"></div>
   
    <div class="paddy">
      <h2>Desarrollo a la Medida</h2>
      <p>
        A través del uso de metodologías ágiles, nuestro equipo trabaja en
        conjunto con el cliente para sacar soluciones tecnológicas que estén
        alineadas a la estrategia del negocio, esto haciendo uso de las
        mejores herramientas de Desarrollo De Software. Desarrollo en
        plataformas winform, web, AS400.
      </p>
      <a class="btn-custom btn-verde" href="<?php echo esc_url( home_url('/desarrollo-a-la-medida') ); ?>">Leer Más</a>
    </div>

    <div class="paddy bg-gris">
      <h2>Asesoría IT</h2>
      <p>Asesoría en elaboración e implementación de Metodologías Agiles de Desarrollo de Software, Ingeniería de Software y Administración de Proyectos. 
      </p>
      <a class="btn-custom btn-rojo" href="<?php echo esc_url( home_url('/asesoria-it') ); ?>">Leer Más</a>
    </div>
    <div class="grid-img-bg wow fadeInRight" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/03.jpg);background-size: cover;"></div>
  </section>

  <div class="gridy">
      <div
      style="
      background: url(<?php echo get_template_directory_uri(); ?>/img/pics/ccc.jpg);
      background-size: cover;
      background-position: center;
      "
      ></div>
	  <style>
		  .suscrip .wpcf7-form-control{
			  padding: .5em 1em;
			  background: white;
			  color: black;
			  border: 1px #2e2858 solid;
		  }
		  .suscrip .wpcf7-submit{
			  background: #2e2858 !important;
			  color: white;
		  }
	  </style>
    <div class="paddy suscrip">
      <h2>Suscríbete a nuestro blog!</h2>
      <p>Así podrás estar enterado de todas nuestras promociones.</p>
      <?php echo do_shortcode('[contact-form-7 id="50" title="suscripcion"]') ?>
    </div>

  </div>





<?php get_footer() ?>