<?php 

get_header();
$linkheader = "https://danielgomez.website/micropos/wp-content/uploads/2019/05/Banner_Retail.jpg";

?>


<div class="cont fixy">
    <img style="width: 100%;" src="<?php echo get_template_directory_uri(); ?>/img/03.jpg" alt="banner">
    <h2 class="title">Asesoría IT</h2>
    <div class="" >
        <div class="" style="">
          <p class="d-flex" style="">
          Asesoría en elaboración e implementación de Metodologías Agiles de Desarrollo de Software, Ingeniería

de Software y Administración de Proyectos.
        </p>
        <p><b>DESCRIPCIÓN:</b></p>
        <ul class="custom-style-type">
              <li>
              Metodologías de Desarrollo de Software, (Programación Extrema XP).
                          </li>
              <li>Ingeniería de Software</li>
              <li> Desarrollo a la medida (Jquery, Arquitectura MVC, Ajax, winform, winnet, PDA,  Mobil)</li>
              <li>Herramientas para el Desarrollo Acelerado de Software, como (Infragistics, Crystal Report)</li>
            </ul>
      </div>
    </div>

</div>
<div style="padding: 5%"></div>

<?php get_footer() ?>